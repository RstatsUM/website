#!/bin/bash
find . -type f -name "*.Rmd" -exec Rscript -e "rmarkdown::render('{}')" \; && bundle exec jekyll $@
