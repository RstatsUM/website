# R users group at the University of Manchester

[Website](https://rstatsum.gitlab.io/website/).

## How to Contribute

[Read our contribution guide](CONTRIBUTING.md).

## How to Build the Website Locally

Run

~~~
jekyll-wrap.sh serve
~~~

from your Bash-compatible terminal
to convert the R Markdown into Markdown files,
create the website
and serve it on [http://localhost:4000](http://localhost:4000).

**If you are using RStudio to edit the R Markdown files
you can convert the files with RStudio
and Jekyll wil update the website for you.**
